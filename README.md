We specialize in buying distressed homes & helping distressed homeowners out of tough situations. We then renovate and resell them to retail home buyers and landlords. Founded in 2017, Equity Pros is excited to be part of the areas renaissance and we aspire to continue contributing to the economic rejuvenation of Wake County and its neighborhoods.

Address : 7365 Sandy Creek Dr, Raleigh, NC 27615

Phone : 919-229-4991